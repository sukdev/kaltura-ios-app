//
//  ViewController.h
//  CCDemo
//
//  Created by Nissim Pardo on 02/06/2016.
//  Copyright © 2016 kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleCast/GoogleCast.h>
#import <KalturaPlayerSDKStreamamg/KPViewController.h>
#import <KalturaPlayerSDKStreamamg/GoogleCastProvider.h>

@interface ViewController : UIViewController <KPlayerDelegate>


@end

