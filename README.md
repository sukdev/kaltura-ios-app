
Please note that this repo is deprecated. You can find new demo app here: https://bitbucket.org/sukdev/streamamg-ios-app-swift/src/master/
======



Getting Started
======

##SDK CocoaPods Installation :

install KalturaPlayerSDKStreamamg. your `Podfile` should look like this:

```
#!ruby

target 'CCDemo' do
# Comment this line if you're not using Swift and don't want to use dynamic frameworks
use_frameworks!

pod 'KalturaPlayerSDKStreamamg', :git => 'https://bitbucket.org/sukdev/kaltura-ios-sdk/src/master/', :branch => 'master'
# Only if you are using Google Ads
pod "GoogleAds-IMA-iOS-SDK" , "~> 3.1.0"

end

```

* KalturaPlayerSDKStreamamg - Video SDK
* GoogleAds-IMA-iOS-SDK - SDK to show ads
* google-cast-sdk - SDK to chromecast

In the terminal navigate to CCDemo folder
```
#!

cd CCDemo
```

Make sure that in podfile you update the git url to KalturaPlayerSDKStreamamg with your bitbucket username.

Install pods, run command:


```
#!

pod install
```